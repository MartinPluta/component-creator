import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import managers.MessagesManager;

import javax.imageio.ImageIO;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main extends Application {

    private Stage stage;

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.stage = primaryStage;
        Parent root = FXMLLoader.load(getClass().getResource("fxml/main.fxml"));
        Platform.setImplicitExit(false);
        primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("assets/tray-icon.png")));
        primaryStage.setTitle("Component Creator");
        primaryStage.setScene(new Scene(root, 800, 500));
        javax.swing.SwingUtilities.invokeLater(this::addAppToTray);
        javax.swing.SwingUtilities.invokeLater(() -> MessagesManager.getTrayIcon().displayMessage(
                "Welcome",
                "Component Creator started to system tray",
                java.awt.TrayIcon.MessageType.INFO
        ));
    }

    private void addAppToTray() {
        try {
            java.awt.Toolkit.getDefaultToolkit();

            if (!java.awt.SystemTray.isSupported()) {
                System.out.println("No system tray support, application exiting.");
                Platform.exit();
            }

            java.awt.SystemTray tray = java.awt.SystemTray.getSystemTray();
            java.awt.Image image = ImageIO.read(getClass().getResource("assets/tray-icon.png"));

            java.awt.TrayIcon trayIcon = new java.awt.TrayIcon(image);
            MessagesManager.setTrayIcon(trayIcon);

            trayIcon.addActionListener(event -> Platform.runLater(this::showStage));

            java.awt.MenuItem newComponentItem = new java.awt.MenuItem("New Component");
            newComponentItem.addActionListener(event -> {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        try

                        {
                            FXMLLoader fxmlLoader = new FXMLLoader();
                            fxmlLoader.setLocation(getClass().getResource("fxml/create-component.fxml"));
                            Scene scene = new Scene(fxmlLoader.load(), 800, 400);
                            Stage stage = new Stage();
                            stage.setTitle("Create Component");
                            stage.setScene(scene);
                            stage.show();
                        } catch(
                                IOException e)

                        {
                            Logger logger = Logger.getLogger(getClass().getName());
                            logger.log(Level.SEVERE, "Failed to create new Window.", e);
                        }
                    }
                });
            });

            java.awt.MenuItem messagesItem = new java.awt.MenuItem("Messages");
            messagesItem.addActionListener(event -> Platform.runLater(this::showStage));

            java.awt.MenuItem settingsItem = new java.awt.MenuItem("Settings");
            settingsItem.addActionListener(event -> {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        try

                        {
                            FXMLLoader fxmlLoader = new FXMLLoader();
                            fxmlLoader.setLocation(getClass().getResource("fxml/settings.fxml"));
                            Scene scene = new Scene(fxmlLoader.load(), 800, 400);
                            Stage stage = new Stage();
                            stage.setTitle("Application Settings");
                            stage.setScene(scene);
                            stage.show();
                        } catch(
                                IOException e)

                        {
                            Logger logger = Logger.getLogger(getClass().getName());
                            logger.log(Level.SEVERE, "Failed to create new Window.", e);
                        }
                    }
                });
            });

            java.awt.Font defaultFont = java.awt.Font.decode(null);
            java.awt.Font boldFont = defaultFont.deriveFont(java.awt.Font.BOLD);
            newComponentItem.setFont(boldFont);

            java.awt.MenuItem exitItem = new java.awt.MenuItem("Exit");
            exitItem.addActionListener(event -> {
                Platform.exit();
                tray.remove(trayIcon);
            });

            final java.awt.PopupMenu popup = new java.awt.PopupMenu();
            popup.add(newComponentItem);
            popup.add(messagesItem);
            popup.add(settingsItem);
            popup.addSeparator();
            popup.add(exitItem);
            trayIcon.setPopupMenu(popup);

            tray.add(trayIcon);
        } catch (java.awt.AWTException | IOException e) {
            System.out.println("Unable to init system tray");
            e.printStackTrace();
        }
    }

    private void showStage() {
        if (stage != null) {
            stage.show();
            stage.toFront();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}