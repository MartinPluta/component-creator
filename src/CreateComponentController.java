import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import managers.FilesystemManager;
import managers.MessagesManager;
import managers.PreferencesManager;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CreateComponentController implements Initializable {

    private ResourceBundle resources;
    @FXML Button createComponent;
    @FXML
    Label componentPrefixLabel;
    @FXML TextField componentType;
    @FXML TextField componentNameText;
    @FXML TextField moduleSubdirectoryText;
    @FXML ListView typeList;
    @FXML
    ToggleGroup componentTypes;
    @FXML
    RadioButton elementRadio;
    @FXML
    RadioButton moduleRadio;

    private boolean isElement = true;
    private boolean isModule = false;
    private String componentName;

    String projectPrefix = PreferencesManager.getPreference("projectPrefix");
    String elementsPrefix = PreferencesManager.getPreference("elementsPrefix");
    String modulesPrefix = PreferencesManager.getPreference("modulesPrefix");

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.resources = resources;
        updateSubdirectoryView();
        addOnTypeListItemClicked();
        String prefix = getComponentNamePrefix();
        componentPrefixLabel.setText(prefix);
        componentTypes.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            public void changed(ObservableValue<? extends Toggle> ov,
                                Toggle old_toggle, Toggle new_toggle) {
                if (elementRadio.isSelected()) {
                    isElement = true;
                    isModule = false;
                    updateSubdirectoryView();
                    String prefix = getComponentNamePrefix();
                    componentPrefixLabel.setText(prefix);
                } else if (moduleRadio.isSelected()){
                    isElement = false;
                    isModule = true;
                    updateSubdirectoryView();
                    String prefix = getComponentNamePrefix();
                    componentPrefixLabel.setText(prefix);
                }
            }
        });

    }

    private void updateSubdirectoryView() {
        String path = getComponentsSubdirectoryPath();
        List<String> directories = FilesystemManager.getSubdirectories(path);
        ObservableList<String> items = FXCollections.observableArrayList (directories);
        typeList.setItems(items);
        return;
    }

    private String getComponentsSubdirectoryPath() {
        if (elementRadio.isSelected()) {
            return PreferencesManager.getPreference("elementsComponentsPath");
        } else if (moduleRadio.isSelected()){
            return PreferencesManager.getPreference("modulesComponentsPath");
        }
        return null;
    }

    private String getStylesSubdirectoryPath() {
        if (elementRadio.isSelected()) {
            return PreferencesManager.getPreference("elementsStylesPath");
        } else if (moduleRadio.isSelected()){
            return PreferencesManager.getPreference("modulesStylesPath");
        }
        return null;
    }

    private void setComponentName(){
        this.componentName = getComponentNamePrefix() + this.componentNameText.getText();
    }

    private String getComponentNamePrefix(){
        if (elementRadio.isSelected()) {
            return projectPrefix + "-" + elementsPrefix + "-";
        } else if (moduleRadio.isSelected()){
            return projectPrefix + "-" + modulesPrefix + "-";
        } else {
            return null;
        }
    }

    public void createComponent(ActionEvent event) throws IOException {
        setComponentName();
        if (isElement) {
            MessagesManager.addMessageAndUpdate("Attempting to create element: " + this.componentName);
            try {
                createElementsComponent();
                createElementsStyle();
            } catch (Exception exception) {
                MessagesManager.addMessageAndUpdate(exception.getMessage(), true);
                createComponent.getScene().getWindow().hide();
                javax.swing.SwingUtilities.invokeLater(() -> MessagesManager.getTrayIcon().displayMessage(
                        "Failed",
                        "Component creation failed",
                        TrayIcon.MessageType.WARNING
                ));
                return;
            }
            injectImportToMainScss();
            MessagesManager.addMessageAndUpdate("Element created:"  + this.componentName);
        } else if (isModule) {
            MessagesManager.addMessageAndUpdate("Attempting to create module:"  + this.componentName);
            try {
                createModuleComponent();
                createModulesStyle();
            } catch (Exception exception) {
                MessagesManager.addMessageAndUpdate(exception.getMessage(), true);
                createComponent.getScene().getWindow().hide();
                javax.swing.SwingUtilities.invokeLater(() -> MessagesManager.getTrayIcon().displayMessage(
                        "Failed",
                        "Component creation failed",
                        TrayIcon.MessageType.WARNING
                ));
                return;
            }
            injectImportToMainScss();
            MessagesManager.addMessageAndUpdate("Module created: "  + this.componentName);
        }
        createComponent.getScene().getWindow().hide();
        javax.swing.SwingUtilities.invokeLater(() -> MessagesManager.getTrayIcon().displayMessage(
                "Success",
                "Component created: " + this.componentName,
                TrayIcon.MessageType.INFO
        ));
    }

    public boolean createModuleComponent() throws IOException {
        String subDirectory = moduleSubdirectoryText.getText();
        String componentName = this.componentName;
        return FilesystemManager.createFileAndDirectoryByPreference("modulesComponentsPath", componentName, ".html", subDirectory);
    }

    public boolean createModulesStyle() throws IOException {
        String subDirectory = moduleSubdirectoryText.getText();
        String componentName = this.componentName;
        return FilesystemManager.createFileAndDirectoryByPreference("modulesStylesPath", componentName, ".scss", subDirectory);
    }

    public boolean createElementsComponent() throws IOException {
        String subDirectory = moduleSubdirectoryText.getText();
        String componentName = this.componentName;
        return FilesystemManager.createFileAndDirectoryByPreference("elementsComponentsPath", componentName, ".html", subDirectory);
    }

    public boolean createElementsStyle() throws IOException {
        String subDirectory = moduleSubdirectoryText.getText();
        String componentName = this.componentName;
        return FilesystemManager.createFileAndDirectoryByPreference("elementsStylesPath", componentName, ".scss", subDirectory);
    }

    public void injectImportToMainScss() throws IOException {
        String mainScssPath = PreferencesManager.getPreference("mainScssPath");
        String targetPath = getStylesSubdirectoryPath();
        String elementStylesDirectory = FilesystemManager.getDirectoryByRegularExpression(targetPath, "styles");
        String subDirectory = moduleSubdirectoryText.getText();
        String componentName = this.componentName;
        String fileName = componentName + ".scss";
        if (!subDirectory.isEmpty()) {
            targetPath = targetPath + File.separator + subDirectory;
        }
        targetPath += File.separator + fileName;
        String relativePath = FilesystemManager.getRelativePath(mainScssPath, targetPath);
        String importStatement = "@import \"" + relativePath + "\";";
        MessagesManager.addMessageAndUpdate("Attempting to add import statement to " + mainScssPath + ": " + importStatement);
        importStatement = FilesystemManager.transformPathFromWindowsToUnix(importStatement);
        addImportToMainScss(subDirectory, importStatement);
    }

    private void addImportToMainScss(String match, String importStatement) throws IOException {
        int elementsStart = 0;
        int modulesStart = 0;
        String section = "";
        MessagesManager.addMessageAndUpdate("Looking for a match of: " + match);
        String mainScssPath = PreferencesManager.getPreference("mainScssPath");
        ArrayList<String> content = FilesystemManager.readFileToList(mainScssPath);
        int position = -1;
        int index = 0;
        for (String line : content) {
            if (matchContent(line,"\\* Elements"))  {
                MessagesManager.addMessageAndUpdate("Elements section detected starting at line: " + index);
                section = "elements";
                elementsStart = index;
            } else if (matchContent(line,"\\* Modules"))  {
                MessagesManager.addMessageAndUpdate("Modules section detected starting at line: " + index);
                section = "modules";
                modulesStart = index;
            }
            String sectionStatement = "// " + match;
                if (matchContent(line, sectionStatement)) {
                    if (section.equals("elements") && isElement) {
                        MessagesManager.addMessageAndUpdate("Found a match in elements at: " + index);
                        MessagesManager.addMessageAndUpdate("Attempting to add to elements: " + match);
                        position = index + 2;
                        break;
                    } else if (section.equals("modules") && isModule) {
                        MessagesManager.addMessageAndUpdate("Found a match in modules: " + index);
                        MessagesManager.addMessageAndUpdate("Attempting to add to modules: " + match);
                        position = index + 2;
                        break;
                    }
                }
            index++;
        }
       if (position < 0) {
           if (isElement) {
               position = elementsStart + 2;
               MessagesManager.addMessageAndUpdate("No match found for group: " + match);
               MessagesManager.addMessageAndUpdate("Attempting to add to elements at position " + position + ": " + match);
           } else if (isModule) {
               position = modulesStart + 2;
               MessagesManager.addMessageAndUpdate("No match found for: " + "// " + match);
               MessagesManager.addMessageAndUpdate("Attempting to add to modules at position " + position + ": " + match);
           }
            content.add(position, "// " + match);
            content.add(position + 1, importStatement);
        } else {
           MessagesManager.addMessageAndUpdate("Found a match at line: " + position + "/" + content.size());
           MessagesManager.addMessageAndUpdate("Attempting to add at position: " + position);
            content.add(position - 1, importStatement);
        }
        FilesystemManager.writeFileFromList(mainScssPath, content);
        MessagesManager.addMessageAndUpdate("Import statement added to: " + mainScssPath);
    }

    private boolean matchContent(String source, String regex) {
        Pattern patterSection = Pattern.compile(regex,
                Pattern.CASE_INSENSITIVE);
        Matcher matcherSection = patterSection.matcher(source);
        return matcherSection.find();
    }

    private void addOnTypeListItemClicked() {
        typeList.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                moduleSubdirectoryText.setText(newValue);
            }
        });
    }
}
