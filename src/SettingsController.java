import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Window;
import managers.FilesystemManager;
import managers.MessagesManager;
import managers.PreferencesManager;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class SettingsController implements Initializable {

    @FXML TextField projectPrefixPathText;
    @FXML TextField modulesPrefixPathText;
    @FXML TextField elementsPrefixPathText;

    @FXML TextField modulesComponentsPathText;
    @FXML TextField elementsComponentsPathText;
    @FXML TextField modulesStylesPathText;
    @FXML TextField elementsStylesPathText;
    @FXML TextField mainScssPathText;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        loadPreferences();
        setOnChangeListeners();
    }

    private void setOnChangeListeners() {
        projectPrefixPathText.textProperty().addListener((obs, oldText, newText) -> {
            PreferencesManager.setPreference("projectPrefix", newText);
        });

        elementsPrefixPathText.textProperty().addListener((obs, oldText, newText) -> {
            PreferencesManager.setPreference("elementsPrefix", newText);
        });

        modulesPrefixPathText.textProperty().addListener((obs, oldText, newText) -> {
            PreferencesManager.setPreference("modulesPrefix", newText);
        });
    }

    private void loadPreferences() {
        String projectPrefix = PreferencesManager.getPreference("projectPrefix");
        String elementsPrefix = PreferencesManager.getPreference("elementsPrefix");
        String modulesPrefix = PreferencesManager.getPreference("modulesPrefix");
        String modulesComponentsPath = PreferencesManager.getPreference("modulesComponentsPath");
        String elementsComponentsPath = PreferencesManager.getPreference("elementsComponentsPath");
        String mainScssPath = PreferencesManager.getPreference("mainScssPath");
        String modulesStylesPath = PreferencesManager.getPreference("modulesStylesPath");
        String elementsStylesPath = PreferencesManager.getPreference("elementsStylesPath");

        projectPrefixPathText.setText(projectPrefix);
        MessagesManager.addMessageAndUpdate("Set project prefix to: " + projectPrefix);
        elementsPrefixPathText.setText(elementsPrefix);
        MessagesManager.addMessageAndUpdate("Set elements prefix to: " + elementsPrefix);
        modulesPrefixPathText.setText(modulesPrefix);
        MessagesManager.addMessageAndUpdate("Set modules prefix to: " + modulesPrefix);

        elementsComponentsPathText.setText(elementsComponentsPath);
        MessagesManager.addMessageAndUpdate("Set elements components path to: " + elementsComponentsPath);
        elementsStylesPathText.setText(elementsStylesPath);
        MessagesManager.addMessageAndUpdate("Set elements styles path to: " + elementsStylesPath);
        modulesComponentsPathText.setText(modulesComponentsPath);
        MessagesManager.addMessageAndUpdate("Set modules components path to: " + modulesComponentsPath);
        modulesStylesPathText.setText(modulesStylesPath);
        MessagesManager.addMessageAndUpdate("Set modules styles path to: " + modulesStylesPath);
        mainScssPathText.setText(mainScssPath);
        MessagesManager.addMessageAndUpdate("Set main scss path to: " + mainScssPath);
    }

    public void auroDetectPaths(ActionEvent event) throws IOException {
        Node source = (Node) event.getSource();
        Window stage = source.getScene().getWindow();
        String path = FilesystemManager.chooseDirectoryToString(stage);
        String elementsComponentsPath = FilesystemManager.getDirectoryByRegularExpression(path, "components.*elements.*");
        PreferencesManager.setPreference("elementsComponentsPath", elementsComponentsPath);
        String elementsStylesPath = FilesystemManager.getDirectoryByRegularExpression(path, "styles.*elements.*");
        PreferencesManager.setPreference("elementsStylesPath", elementsStylesPath);
        String modulesComponentsPath = FilesystemManager.getDirectoryByRegularExpression(path, "components.*modules.*");
        PreferencesManager.setPreference("modulesComponentsPath", modulesComponentsPath);
        String modulesStylesPath = FilesystemManager.getDirectoryByRegularExpression(path, "styles.*modules.*");
        PreferencesManager.setPreference("modulesStylesPath", modulesStylesPath);
        loadPreferences();
    }

    public void chooseModulesComponentsPath(MouseEvent event) {
        Node source = (Node) event.getSource();
        updateDirectoryPath(source, "modulesComponentsPath", modulesComponentsPathText);
    }

    public void chooseElementsComponentsPath(MouseEvent event) {
        Node source = (Node) event.getSource();
        updateDirectoryPath(source, "elementsComponentsPath", elementsComponentsPathText);
    }

    public void chooseMainScssFilePath(MouseEvent event) {
        Node source = (Node) event.getSource();
        updateFilePath(source, "mainScssPath", mainScssPathText);
    }

    public void chooseModulesStylesPath(MouseEvent event) {
        Node source = (Node) event.getSource();
        updateDirectoryPath(source, "modulesStylesPath", modulesStylesPathText);
    }

    public void chooseElementsStylesPath(MouseEvent event) {
        Node source = (Node) event.getSource();
        updateDirectoryPath(source, "elementsStylesPath", elementsStylesPathText);
    }

    private void updateFilePath(Node source, String preferenceKey, TextField textField) {
        Window stage = source.getScene().getWindow();
        String path = FilesystemManager.chooseFileToString(stage);
        PreferencesManager.setPreference(preferenceKey, path);
        textField.setText(path);
    }

    private void updateDirectoryPath(Node source, String preferenceKey, TextField textField) {
        Window stage = source.getScene().getWindow();
        String path = FilesystemManager.chooseDirectoryToString(stage);
        PreferencesManager.setPreference(preferenceKey, path);
        textField.setText(path);
    }
}
