package managers;

import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Window;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FilesystemManager {

    public static boolean createFileAndDirectoryByPreference(String preferenceKey, String componentName, String fileType, String subDirectory) throws IOException {
        String path = PreferencesManager.getPreference(preferenceKey);
        String fileName = componentName + fileType;
        if (!subDirectory.isEmpty()) {
            path = path + File.separator + subDirectory;
        }
        new File(path).mkdirs();
        path += File.separator + fileName;
        File file = new File(path);
        if (!file.exists()) {
            file.createNewFile();
            MessagesManager.addMessageAndUpdate("File created at: " + path);
            return true;
        } else {
            throw new FileAlreadyExistsException("File creation failed. File already exists at: " + path);
        }
    }

    public static String getDirectoryByRegularExpression(String root, String regex) throws IOException {
        List<File> directories = getAllSubdirectories(root);
        for (File directory : directories) {
            if (RegularExpressionsManager.matchContent(directory.toString(), regex)) {
                return directory.getAbsolutePath();
            }
        }
        return "";
    }

   public static List<File> getAllSubdirectories(String root) throws IOException {
        File file = new File(root);
        List<File> subdirs = Arrays.asList(file.listFiles(new FileFilter() {
            public boolean accept(File f) {
                return f.isDirectory();
            }
        }));
        subdirs = new ArrayList<File>(subdirs);

        List<File> deepSubdirs = new ArrayList<File>();
        for(File subdir : subdirs) {
            deepSubdirs.addAll(getAllSubdirectories(subdir.getAbsolutePath().toString()));
        }
        subdirs.addAll(deepSubdirs);
        return subdirs;
    }

    public static List<String> getSubdirectories(String path) {
        File file = new File(path);
        String[] directories = file.list(new FilenameFilter() {
            @Override
            public boolean accept(File current, String name) {
                return new File(current, name).isDirectory();
            }
        });

        return Arrays.asList(directories);
    }

    public static String chooseFileToString(Window stage) {
        FileChooser fileChooser = new FileChooser();
        File file =
                fileChooser.showOpenDialog(stage);
        if(file != null){
            return file.getPath();
        } else {
            return null;
        }
    }

    public static String chooseDirectoryToString(Window stage) {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        File selectedDirectory =
                directoryChooser.showDialog(stage);
        if(selectedDirectory != null){
             return selectedDirectory.getPath();
        } else {
            return null;
        }
    }

    public static String readFile(String path) throws IOException {
        return new String(Files.readAllBytes(Paths.get(path)));
    }

    public static ArrayList<String> readFileToList(String path) throws IOException {
        ArrayList<String> lines = new ArrayList<String>();
        List<String> source = Files.readAllLines(Paths.get(path));
        lines.addAll(source);
        return lines;
    }

    public static void writeFileFromList(String path, List<String> content) throws IOException {
        Files.write(Paths.get(path), content);
    }

    public static String getRelativePath(String base, String target) {
        Path targetPath = Paths.get(target);
        Path basePath = Paths.get(base);
        return basePath.relativize(targetPath).toString();
    }

    public static String transformPathFromWindowsToUnix(String path) {
        return path.replace("\\", "/").replace("..", ".");
    }
}
