package managers;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by mpluta on 25.07.17.
 */
public class RegularExpressionsManager {

    public static boolean matchContent(String source, String regex) {
        Pattern patterSection = Pattern.compile(regex,
                Pattern.CASE_INSENSITIVE);
        Matcher matcherSection = patterSection.matcher(source);
        return matcherSection.find();
    }

}
