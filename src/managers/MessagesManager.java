package managers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ListView;

import java.awt.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class MessagesManager {

    private static TrayIcon trayIcon;

    private static ListView messagesView;
    private static ArrayList<String> messages = new ArrayList<String>();

    private static ListView errorMessagesView;
    private static ArrayList<String> errorMessages = new ArrayList<String>();

    public static void addErrorMessageAndUpdate(String message) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        errorMessages.add(0, "[ " + dateFormat.format(date) + " ]   " + message);
        updateErrorMessageView();
    }

    public static void updateErrorMessageView() {
        ObservableList<String> items = FXCollections.observableArrayList(errorMessages);
        errorMessagesView.setItems(items);
    }

    public static void addMessageAndUpdate(String message) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        messages.add(0, "[ " + dateFormat.format(date) + " ]   " + message);
        updateMessageView();
    }

    public static void addMessageAndUpdate(String message, boolean addToErrorMessages) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        messages.add(0, "[ " + dateFormat.format(date) + " ]   " + message);
        updateMessageView();
        if (addToErrorMessages) {
            addErrorMessageAndUpdate(message);
        }
    }

    public static void updateMessageView() {
        ObservableList<String> items = FXCollections.observableArrayList(messages);
        messagesView.setItems(items);
    }

    public static ArrayList<String> getErrorMessages() {
        return errorMessages;
    }

    public static void setErrorMessages(ArrayList<String> errorMessages) {
        MessagesManager.errorMessages = errorMessages;
    }

    public static ListView getErrorMessagesView() {
        return errorMessagesView;
    }

    public static void setErrorMessagesView(ListView errorMessagesView) {
        MessagesManager.errorMessagesView = errorMessagesView;
    }

    public static ListView getMessagesView() {
        return messagesView;
    }

    public static void setMessagesView(ListView messagesView) {
        MessagesManager.messagesView = messagesView;
    }

    public static ArrayList<String> getMessages() {
        return messages;
    }

    public static void setMessages(ArrayList<String> messages) {
        MessagesManager.messages = messages;
    }

    public static TrayIcon getTrayIcon() {
        return trayIcon;
    }

    public static void setTrayIcon(TrayIcon trayIcon) {
        MessagesManager.trayIcon = trayIcon;
    }
}
