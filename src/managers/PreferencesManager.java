package managers;

import java.util.prefs.Preferences;

/**
 * Created by mpluta on 24.07.17.
 */
public class PreferencesManager {

    static final String projectPath = "projectPath";

    public static String getPreference(String key) {
        Preferences preferences = Preferences.userRoot().node("component-creator");
        return preferences.get(key, "");
    }

    public static void setPreference(String key, String value) {
        Preferences preferences = Preferences.userRoot().node("component-creator");
        preferences.put(key, value);
    }

}
